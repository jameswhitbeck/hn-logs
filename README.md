# HN log indexing #

This is a Java Web App running Jersey and deployable to the Google AppEngine. It indexes HN query
data and exposes endpoints about the queries.

### Implementations ###

There are 4 implementations for the indexing portion of this project.

```ScanningQueryIndex.java``` and ```SortedListQueryIndex.java``` are very basic implementations that 
do very little pre-computation but a lot of aggregation at query time.

```PrecomputedPrefixHashMapQueryIndex.java``` pre-computes all possible prefixes. It's also a pretty simple implementation.
Predictably, it is blazing fast on queries but has the largest memory footprint.

```SparsePrefixTreeQueryIndex.java``` also pre-computes everything that's needed to answer requests, but it's smart
 about not computing redundant prefixes. As a result, it has similar query performance to ```PrecomputedPrefixHashMapQueryIndex.java```
 and has a lower memory footporint. 
 This particular implementation also supports online indexing of new data,
 in almost constant time (there is a bit of tree traversal to do). 

### Benchmarking ###

Implementations were benchmarked in the benchmark method of QueryIndexTest.java. 
Memory footprint was roughly computed from running ```top``` when the devserver was running.
From the table below, it's clear that the precomputed indices are a lot faster, by 5 orders of magnitude.
However, they do take up more memory.

|        Index Implementation        | Memory Footprint | getCount avg latency | getPopular avg latency |
|:----------------------------------:|:----------------:|:--------------------:|:----------------------:|
| ScanningQueryIndex                 |             765M | 812 ms               | 933 ms                 |
| SortedListQueryIndex               |            1035M | 485 ms               | 710 ms                 |
| SparsePrefixTreeQueryIndex         |            2036M | 0.0032 ms            | 0.0038 ms              |
| PrecomputedPrefixHashMapQueryIndex |            2876M | 0.0015 ms            | 0.0031 ms              |

### Running the code ###

* ```mvn clean```
* ```mvn -DskipTests=true appengine:devserver```

### Running tests ###

* ```mvn test``` (note: this can take a while because QueryIndexTest load the full data set)

### Deploying the code to Google App Engine ###

* In appengine-web.xml, make sure ```<application>hn-logs</application>``` contains your Google application name
* ```mvn -DskipTests=true appengine:update```
* Then you can play around: ```curl -i -HAccept:application/json https://hn-logs.appspot.com/1/queries/popular/2015/?size=5```
* Note 1: I will only have the app deployed for a limited time.
* Note 2: Given memory limitations in Google AppEngine, I have deployed a version that uses a slower but 
 leaner index (```SortedListQueryIndex.java```).

### Example queries ###

* ```curl -i -HAccept:application/json http://localhost:8080/1/queries/count/2015```
* ```curl -i -HAccept:application/json http://localhost:8080/1/queries/count/2015-08```
* ```curl -i -HAccept:application/json http://localhost:8080/1/queries/count/2015-08-03```
* ```curl -i -HAccept:application/json http://localhost:8080/1/queries/count/2015-08-01 00:04```
* ```curl -i -HAccept:application/json http://localhost:8080/1/queries/popular/2015?size=3```
* ```curl -i -HAccept:application/json http://localhost:8080/1/queries/popular/2015-08-02?size=5```
