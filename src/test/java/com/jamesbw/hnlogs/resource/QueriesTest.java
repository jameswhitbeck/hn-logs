package com.jamesbw.hnlogs.resource;

import com.jamesbw.hnlogs.index.QueryIndex;
import com.jamesbw.hnlogs.validation.DateValidator;
import java.util.List;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class QueriesTest {

  private Queries queries;
  @Mock private QueryIndex queryIndex;
  @Mock private DateValidator dateValidator;
  @Rule public ExpectedException expectedException = ExpectedException.none();

  private String someInvalidPrefix = "invalidPrefix";
  private String someValidPrefix = "validPrefix";
  private Integer someValidSize = 123;
  private Integer nullSize = null;
  private int expectedGetCountResult = 332;
  @Mock private List<QueryIndex.QueryCount> expectedGetPopularResult;

  @org.junit.Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    when(dateValidator.isValidDatePrefix(someValidPrefix)).thenReturn(true);
    when(dateValidator.isValidDatePrefix(someInvalidPrefix)).thenReturn(false);

    when(queryIndex.getCount(someValidPrefix)).thenReturn(expectedGetCountResult);
    when(queryIndex.getPopular(someValidPrefix, someValidSize)).thenReturn(expectedGetPopularResult);

    queries = new Queries(queryIndex, dateValidator);
  }

  @Test
  public void testGetCountValidatesDatePrefix() {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage("invalidPrefix is not a valid date prefix");
    queries.getCount(someInvalidPrefix);
    verify(dateValidator).isValidDatePrefix(someInvalidPrefix);
  }

  @Test
  public void testGetCount() {
    assertThat(queries.getCount(someValidPrefix)).isEqualTo(new Queries.CountResult(expectedGetCountResult));
    verify(dateValidator).isValidDatePrefix(someValidPrefix);
    verify(queryIndex).getCount(someValidPrefix);
  }

  @Test
  public void testGetPopularValidatesDatePrefix() {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage("invalidPrefix is not a valid date prefix");
    queries.getPopular(someInvalidPrefix, someValidSize);
    verify(dateValidator).isValidDatePrefix(someInvalidPrefix);
  }

  @Test
  public void testGetPopularValidatesNonNullSize() {
    expectedException.expect(RuntimeException.class);
    expectedException.expectMessage("Size param:null must be specified as an integer");
    queries.getPopular(someValidPrefix, nullSize);
    verify(dateValidator).isValidDatePrefix(someValidPrefix);
  }

  @Test
  public void testGetPopular() {
    assertThat(queries.getPopular(someValidPrefix, someValidSize)).isEqualTo(expectedGetPopularResult);
    verify(dateValidator).isValidDatePrefix(someValidPrefix);
    verify(queryIndex).getPopular(someValidPrefix, someValidSize);
  }
}
