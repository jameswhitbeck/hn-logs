package com.jamesbw.hnlogs.validation;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DateValidatorTest {

  private final DateValidator dateValidator = new DateValidator();

  @Test public void testValidPatterns() {
    List<String> validDatePrefixes = Arrays.asList(
        "2015-08-04 00:00:00",
        "2015-08-04 00:00:0",
        "2015-08-04 00:00:",
        "2015-08-04 00:00",
        "2015-08-04 00:0",
        "2015-08-04 00",
        "2015-08-04 0",
        "2015-08-04 ",
        "2015-08-04",
        "2015-08-0",
        "2015-08-",
        "2015-08",
        "2015-0",
        "2015-",
        "2015",
        "201",
        "20",
        "2",
        "");

    for (String validDatePrefix : validDatePrefixes) {
      assertThat(dateValidator.isValidDatePrefix(validDatePrefix)).isTrue();
    }
  }

  @Test public void testInvalidPatterns() {
    List<String> invalidDatePrefixes = Arrays.asList(
        "2015-18-04 00:00:00",
        "2015-0804 00:00:0",
        "201508",
        "2015-08-0400:00",
        "2015-08-04 000",
        "2015-08-04 00:00:0000");

    for (String invalidDatePrefix : invalidDatePrefixes) {
      assertThat(dateValidator.isValidDatePrefix(invalidDatePrefix)).isFalse();
    }
  }
}
