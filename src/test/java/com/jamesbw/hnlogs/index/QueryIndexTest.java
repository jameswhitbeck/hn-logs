package com.jamesbw.hnlogs.index;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class QueryIndexTest {

  // static so they only get built once.
  private final static List<QueryIndex> queryIndexImpls = Arrays.<QueryIndex>asList(
      new ScanningQueryIndex(),
      new SortedListQueryIndex(),
      new PrecomputedPrefixHashMapQueryIndex(),
      new SparsePrefixTreeQueryIndex()
  );

  @Test
  public void testGetPopular() {
    for (QueryIndex queryIndexImpl : queryIndexImpls) {
      System.out.println("Testing getPopular on " + queryIndexImpl.getClass().getSimpleName());

      long start = System.nanoTime();

      assertThat(queryIndexImpl.getPopular("2016", 3)).isEmpty();
      assertThat(queryIndexImpl.getPopular("2015", 0)).isEmpty();
      assertThat(queryIndexImpl.getPopular("2015", 3)).containsExactly(
          new QueryIndex.QueryCount("http%3A%2F%2Fwww.getsidekick.com%2Fblog%2Fbody-language-advice", 6675),
          new QueryIndex.QueryCount("http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F568045", 4652),
          new QueryIndex.QueryCount("http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F379035%3Fsort%3D1", 3100)
      );
      assertThat(queryIndexImpl.getPopular("2015-08-02", 5)).containsExactly(
          new QueryIndex.QueryCount("http%3A%2F%2Fwww.getsidekick.com%2Fblog%2Fbody-language-advice", 2283),
          new QueryIndex.QueryCount("http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F568045", 1943),
          new QueryIndex.QueryCount("http%3A%2F%2Fwebboard.yenta4.com%2Ftopic%2F379035%3Fsort%3D1", 1358),
          new QueryIndex.QueryCount("http%3A%2F%2Fjamonkey.com%2F50-organizing-ideas-for-every-room-in-your-house%2F", 890),
          new QueryIndex.QueryCount("http%3A%2F%2Fsharingis.cool%2F1000-musicians-played-foo-fighters-learn-to-fly-and-it-was-epic", 701)
      );

      long end = System.nanoTime();
      System.out.println(String.format("Tested getPopular on %s in %s millis",
          queryIndexImpl.getClass().getSimpleName(), (end - start)/1e6));
    }
  }

  @Test
  public void testGetCount() {
    for (QueryIndex queryIndexImpl : queryIndexImpls) {
      System.out.println("Testing getCount on " + queryIndexImpl.getClass().getSimpleName());

      long start = System.nanoTime();

      assertThat(queryIndexImpl.getCount("2016")).isEqualTo(0);
      assertThat(queryIndexImpl.getCount("")).isEqualTo(573697);
      assertThat(queryIndexImpl.getCount("2015")).isEqualTo(573697);
      assertThat(queryIndexImpl.getCount("2015-08")).isEqualTo(573697);
      assertThat(queryIndexImpl.getCount("2015-08-03")).isEqualTo(198117);
      assertThat(queryIndexImpl.getCount("2015-08-01 00:04")).isEqualTo(617);

      long end = System.nanoTime();
      System.out.println(String.format("Tested getCount on %s in %s millis",
          queryIndexImpl.getClass().getSimpleName(), (end - start)/1e6));
    }
  }

  @Test
  public void benchmark() {
    int numIter = 5;
    List<String> requestPrefixes = Arrays.asList(
        "",
        "2016",
        "2015",
        "2015-08",
        "2015-08-03",
        "2015-08-03 01",
        "2015-08-03 01:01",
        "2015-08-03 01:01:01"
    );
    List<Integer> sizes = Arrays.asList(0,3,10,30);

    for (QueryIndex queryIndexImpl : queryIndexImpls) {
      int getCountCount = 0;
      long getCountStart = System.nanoTime();
      for (int i = 0; i < numIter; i++) {
        for (String requestPrefix : requestPrefixes) {
          queryIndexImpl.getCount(requestPrefix);
          getCountCount++;
        }
      }
      long getCountEnd = System.nanoTime();

      int getPopularCount = 0;
      long getPopularStart = System.nanoTime();
      for (int i = 0; i < numIter; i++) {
        for (String requestPrefix : requestPrefixes) {
          for (Integer size : sizes) {
            queryIndexImpl.getPopular(requestPrefix, size);
            getPopularCount++;
          }
        }
      }
      long getPopularEnd = System.nanoTime();

      System.out.println(String.format("%s: getCount: %s millis, getPopular: %s millis",
          queryIndexImpl.getClass().getSimpleName(),
          (getCountEnd - getCountStart) / (getCountCount * 1e6),
          (getPopularEnd - getPopularStart) / (getPopularCount * 1e6)));
    }
  }
}
