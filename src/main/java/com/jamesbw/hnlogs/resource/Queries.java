package com.jamesbw.hnlogs.resource;

import com.jamesbw.hnlogs.index.QueryIndex;
import com.jamesbw.hnlogs.index.QueryIndex.QueryCount;
import com.jamesbw.hnlogs.index.SparsePrefixTreeQueryIndex;
import com.jamesbw.hnlogs.validation.DateValidator;
import java.util.List;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/1/queries")
@Singleton // singleton to avoid rebuilding index on every request
public class Queries {

  private final QueryIndex queryIndex;
  private final DateValidator dateValidator;

  public Queries() {
    this(new SparsePrefixTreeQueryIndex(), new DateValidator());
  }

  public Queries(QueryIndex queryIndex, DateValidator dateValidator) {
    this.queryIndex = queryIndex;
    this.dateValidator = dateValidator;
  }

  @GET @Path("/count/{datePrefix}")
  @Produces("application/json")
  public CountResult getCount(@PathParam("datePrefix") String datePrefix) {
    if (!dateValidator.isValidDatePrefix(datePrefix)) {
      throw new RuntimeException(String.format("%s is not a valid date prefix", datePrefix));
    }

    long start = System.nanoTime();
    CountResult result = new CountResult(queryIndex.getCount(datePrefix));
    long end = System.nanoTime();
    System.out.println(String.format("GetCount query for date prefix: %s took %s millis",
        datePrefix, (end - start) / 1e6));
    return result;
  }

  @GET @Path("/popular/{datePrefix}")
  @Produces("application/json")
  public List<QueryCount> getPopular(@PathParam("datePrefix") String datePrefix,
      @QueryParam("size") Integer size) {

    if (!dateValidator.isValidDatePrefix(datePrefix)) {
      throw new RuntimeException(String.format("%s is not a valid date prefix", datePrefix));
    }

    if (size == null) {
      throw new RuntimeException(String.format("Size param:%s must be specified as an integer", size));
    }

    long start = System.nanoTime();
    List<QueryCount> result = queryIndex.getPopular(datePrefix, size);
    long end = System.nanoTime();
    System.out.println(String.format("GetPopular query for date prefix: %s, size: %s took %s millis",
        datePrefix, size, (end - start) / 1e6));
    return result;
  }

  public static class CountResult {
    public final int count;

    public CountResult(int count) {
      this.count = count;
    }

    // equals() and hashCode() are overridden for testing purposes.
    @Override public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      CountResult that = (CountResult) o;

      return count == that.count;
    }

    @Override public int hashCode() {
      return count;
    }
  }
}
