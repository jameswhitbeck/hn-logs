package com.jamesbw.hnlogs.index;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * Just loads the data from the gzip and makes it available as a list.
 */
public class DataLoader {
  static class DataEntry {
    final String timestamp;
    final String query;

    DataEntry(String timestamp, String query) {
      this.timestamp = timestamp;
      this.query = query;
    }
  }

  List<DataEntry> loadData(String... fileNames) {
    List<DataEntry> result = new LinkedList<>();
    try {
      for (String fileName : fileNames) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
            new GZIPInputStream(getClass().getClassLoader().getResourceAsStream(fileName))));
        for (;;) {
          String line = bufferedReader.readLine();
          if (line == null) break;
          String[] splitLine = line.split("\t");
          result.add(new DataEntry(splitLine[0], splitLine[1]));
        }
      }
    } catch (IOException e) {
      // Just wrapping the exception to avoid having to declare it everywhere.
      throw new RuntimeException(e);
    }
    return result;
  }
}
