package com.jamesbw.hnlogs.index;

import com.jamesbw.hnlogs.index.DataLoader.DataEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A slightly smarter implementation than ScanningQueryIndex in that it sorts the queries by time,
 * so that only a given range needs to be scanned on each request.
 */
public class SortedListQueryIndex implements QueryIndex {

  private final ArrayList<DataEntry> entries;

  public SortedListQueryIndex() {
    this(new DataLoader().loadData("hn_logs_1.tsv.gz", "hn_logs_2.tsv.gz"));
  }

  public SortedListQueryIndex(List<DataEntry> dataEntries) {
    entries = new ArrayList<>(dataEntries);
    Collections.shuffle(entries); // just because the HN data is already sorted...
    Collections.sort(entries, new Comparator<DataEntry>() {
      @Override public int compare(DataEntry o1, DataEntry o2) {
        return o1.timestamp.compareTo(o2.timestamp);
      }
    });
  }

  @Override public List<QueryCount> getPopular(String requestPrefix, int size) {
    int startingIndex = findFirstIndexMatchingPrefix(requestPrefix);
    if (startingIndex == -1) return Collections.emptyList();

    Map<String, Integer> queryCounts = new HashMap<>();
    for (int index = startingIndex; index < entries.size()
        && entries.get(index).timestamp.startsWith(requestPrefix); index++) {
      DataEntry current = entries.get(index);
      if (queryCounts.containsKey(current.query)) {
        queryCounts.put(current.query, queryCounts.get(current.query) + 1);
      } else {
        queryCounts.put(current.query, 1);
      }
    }
    List<QueryCount> results = new ArrayList<>();
    for (String query : queryCounts.keySet()) {
      results.add(new QueryCount(query, queryCounts.get(query)));
    }

    Collections.sort(results, new Comparator<QueryCount>() {
          @Override public int compare(QueryCount o1, QueryCount o2) {
            return Integer.compare(o2.count, o1.count);
          }
        }
    );
    return results.subList(0, Math.min(size, results.size()));
  }

  @Override public int getCount(String requestPrefix) {
    int startingIndex = findFirstIndexMatchingPrefix(requestPrefix);
    if (startingIndex == -1) return 0;

    Set<String> queries = new HashSet<>();
    for (int index = startingIndex; index < entries.size()
        && entries.get(index).timestamp.startsWith(requestPrefix); index++) {
      queries.add(entries.get(index).query);
    }
    return queries.size();
  }

  @Override public void indexEntry(String time, String query) {
    // This could be supported, but would require inserting into the list every time.
    // This could be made feasible with a skip list or tree map, but not worth the trouble.
    throw new RuntimeException("::indexEntry is not supported on SortedListQueryIndex");
  }

  // Returns -1 if not found.
  private int findFirstIndexMatchingPrefix(String requestPrefix) {
    int index = entries.size() / 2;
    int step = Math.max(1, entries.size() / 4);
    while (true) {
      String timestamp = entries.get(index).timestamp;
      if (timestamp.startsWith(requestPrefix)) {
        if (index == 0) return index;
        if (!entries.get(index - 1).timestamp.startsWith(requestPrefix)) return index;
        index -= step;
      } else {
        if (index == entries.size() - 1) return -1;
        if (timestamp.compareTo(requestPrefix) < 0) {
          index += step;
        } else {
          index -= step;
        }
      }
      step = Math.max(1, step / 2);
    }
  }
}
