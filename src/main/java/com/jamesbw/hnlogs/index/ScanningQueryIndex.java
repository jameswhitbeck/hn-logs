package com.jamesbw.hnlogs.index;

import com.jamesbw.hnlogs.index.DataLoader.DataEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A very simplistic implementation that scans the whole data set to answer each request.
 */
public class ScanningQueryIndex implements QueryIndex {

  private final List<DataEntry> entries;

  public ScanningQueryIndex() {
    this(new DataLoader().loadData("hn_logs_1.tsv.gz", "hn_logs_2.tsv.gz"));
  }

  public ScanningQueryIndex(List<DataEntry> dataEntries) {
    entries = dataEntries;
  }

  @Override
  public List<QueryCount> getPopular(String requestPrefix, int size) {
    Map<String, Integer> queryCounts = new HashMap<>();
    for (DataEntry entry : entries) {
      if (entry.timestamp.startsWith(requestPrefix)) {
        if (queryCounts.containsKey(entry.query)) {
          queryCounts.put(entry.query, queryCounts.get(entry.query) + 1);
        } else {
          queryCounts.put(entry.query, 1);
        }
      }
    }
    List<QueryCount> results = new ArrayList<>();
    for (String query : queryCounts.keySet()) {
      results.add(new QueryCount(query, queryCounts.get(query)));
    }

    Collections.sort(results, new Comparator<QueryCount>() {
          @Override public int compare(QueryCount o1, QueryCount o2) {
            return Integer.compare(o2.count, o1.count);
          }
        }
    );
    return results.subList(0, Math.min(size, results.size()));
  }

  @Override
  public int getCount(String requestPrefix) {
    Set<String> queries = new HashSet<>();
    for (DataEntry entry : entries) {
      if (entry.timestamp.startsWith(requestPrefix)) {
        queries.add(entry.query);
      }
    }
    return queries.size();
  }

  @Override
  public void indexEntry(String time, String query) {
    entries.add(new DataEntry(time, query));
  }
}
