package com.jamesbw.hnlogs.index;

import com.google.common.base.Preconditions;
import com.jamesbw.hnlogs.index.DataLoader.DataEntry;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;

/**
 * A tree based index, where we only pre-compute the counts for prefixes where the data branches.
 *
 * The advantages over the fully precomputed index are:
 * - less storage if the prefixes can be very long and high cardinality (this is not really the case
 *   with timestamps)
 *
 * Disadvantages are:
 * - slightly slower lookups, because we need a tree traversal instead of a hashmap lookup.
 *
 * This implementation also supports adding new elements to the index in almost constant time
 * (constant for a given prefix) instead of computing everything at once.
 * This has the benefit of being more useful in a live system, but the downside of requiring more
 * complex and heavy data structures.
 *
 * Details:
 * The overall structure is a tree where each node has a prefix which is itself the prefix of all the children
 * We maintain the property that no children can have a common prefix longer than the parent's prefix.
 * If that ever happens, we group those children under a new node with the common prefix. By maintaining
 * this property, we ensure that no request on the index needs to aggregate any data, since all
 * common prefixes from the data are precomputed.
 *
 * Each tree node contains all the aggregated counts per query for timestamps that a contained in the
 * node's prefix.
 * These counts are sorted in decreasing order in a doubly linked list. When a query is incremented,
 * we use a hashmap of query -> count to increment the count in constant time.
 * To maintain the order in the linked list, we move the element that got incremented so that it is
 * before all the elements that had the old count. We do this in constant time through a hashmap that
 * links every count to the first element in the linked list with that count.
 */
public class SparsePrefixTreeQueryIndex implements QueryIndex {

  // We make a root with no prefix. This makes recursion easier and keeps the root stable over time.
  // This comes at the cost of storage cost at the root and a little more traversal time.
  private PrefixTreeNode root = new PrefixTreeNode("");

  public SparsePrefixTreeQueryIndex() {
    this(new DataLoader().loadData("hn_logs_1.tsv.gz", "hn_logs_2.tsv.gz"));
  }

  public SparsePrefixTreeQueryIndex(List<DataEntry> dataEntries) {
    long start = System.currentTimeMillis();
    for (DataEntry dataEntry : dataEntries) {
      indexEntry(dataEntry.timestamp, dataEntry.query);
    }
    long end = System.currentTimeMillis();
    System.out.println(String.format("Indexing finished for %s in %.3f seconds",
        this.getClass().getSimpleName(), (end - start) / 1000.0));
  }

  @Override public List<QueryCount> getPopular(String requestPrefix, int size) {
    PrefixTreeNode nodeForPrefix = getNodeForPrefix(requestPrefix);
    if (nodeForPrefix == null) {
      return Collections.emptyList();
    } else {
      return nodeForPrefix.top(size);
    }
  }

  @Override public int getCount(String requestPrefix) {
    PrefixTreeNode nodeForPrefix = getNodeForPrefix(requestPrefix);
    if (nodeForPrefix == null) {
      return 0;
    } else {
      return nodeForPrefix.count();
    }
  }

  // This method will traverse the tree to find the node with the data for that prefix.
  // It will be used by both ::getPopular and ::getCount
  private PrefixTreeNode getNodeForPrefix(String requestPrefix) {
    return root.getNodeForPrefix(requestPrefix);
  }

  private final Object indexLock = new Object();

  // note: this implementation of indexEntry is not threadsafe
  @Override
  public void indexEntry(String time, String query) {
    synchronized (indexLock) {
      root.insert(time, query);
    }
  }

  /**
   * A tree, where each node contains a prefix, counts of all the queries whose times start with
   * that prefix and a collection of children nodes whose prefixes all start with this node's prefix.
   */
  private static class PrefixTreeNode {

    private final String prefix;
    private final Map<String, QueryListNode> queryCounts = new HashMap<>();

    // This maps query counts to the first element in the sorted linked list of query counts
    // with the given count. This allows moving elements with new counts to the right place in the list
    // in constant time.
    private final Map<Integer, QueryListNode> countToQueryList = new HashMap<>();

    // This maps the children nodes' prefixes to the nodes themselves
    // It's a NavigableMap so that the keys are sorted and one can find the keys immediately before
    // or after a given requestPrefix in O(logN). TreeMaps are balanced red-black trees.
    private final NavigableMap<String, PrefixTreeNode> prefixToChildren = new TreeMap<>();

    // This is a doubly-linked list of queries, sorted in descending order of count.
    private QueryListNode countListHead;
    private QueryListNode countListTail;

    private PrefixTreeNode(String prefix) {
      this.prefix = prefix;
    }

    // Returns a new tree node with a deep copy of the data and a new prefix
    // Used to consolidate common prefixes
    private PrefixTreeNode deepCopy(String newPrefix) {
      PrefixTreeNode newNode = new PrefixTreeNode(newPrefix);

      for (QueryListNode listNode = this.countListHead; listNode != null; listNode = listNode.next) {
        // the newNode.new is necessary, as opposed to new, so that the queryListNode is scoped
        // in the correct tree node and has access to the right head and tail references.
        QueryListNode copiedNode = newNode.new QueryListNode(listNode.query, listNode.count);
        newNode.enqueue(copiedNode);
        if (!newNode.countToQueryList.containsKey(listNode.count)) {
          newNode.countToQueryList.put(listNode.count, copiedNode);
        }
        newNode.queryCounts.put(listNode.query, copiedNode);
      }
      return newNode;
    }

    // Increments the count of a particular query. This will update both queryCounts and countsToQueryList.
    private void incrementQueryCount(String query) {
      if (!queryCounts.containsKey(query)) {
        // If this query hasn't been seen yet, we'll add a new node for it at the end of the linked
        // list of counts.
        QueryListNode newNode = new QueryListNode(query, 1);
        enqueue(newNode);

        // We'll also add that node to the queryCounts map.
        queryCounts.put(query, newNode);

        // Also point the count 1 to this new node if there are no nodes with count 1 yet.
        if (!countToQueryList.containsKey(1)) {
          countToQueryList.put(1, newNode);
        }
      } else {
        // We will update the node's count and update the linked list to maintain order
        QueryListNode node = queryCounts.get(query);
        int oldCount = node.count;
        int newCount = ++node.count;

        // Move node to right place in linked list. The right place is just before all nodes with
        // count == oldCount. The first such node is pointed to by countToQueryList[oldCount].

        // Case 1: node is already first in sublist with count == oldCount
        // In this case, node doesn't need to move. We just update the maps
        if (countToQueryList.get(oldCount) == node) {
          if (!countToQueryList.containsKey(newCount)) {
            countToQueryList.put(newCount, node);
          }
          if (node.next == null || node.next.count != oldCount) {
            countToQueryList.remove(oldCount); // remove oldCount from map if no nodes have this count.
          } else {
            countToQueryList.put(oldCount, node.next);
          }
        } else {
          // Case 2: node is not first in list with count == oldCount
          // In this case, we don't need to update countToQueryList[oldCount].
          // We do need to move the node in front of countToQueryList[oldCount] in the linked list
          // We also set countToQueryList[newCount] if it was not set
          node.moveAheadOf(countToQueryList.get(oldCount));
          if (!countToQueryList.containsKey(newCount)) {
            countToQueryList.put(newCount, node);
          }
        }
      }
    }

    // Adds a node at the end of the list
    private void enqueue(QueryListNode node) {
      if (countListTail == null) {
        countListHead = node;
        countListTail = node;
      } else {
        countListTail.next = node;
        node.prev = countListTail;
        countListTail = node;
      }
    }

    // Insertion involves traversing the tree through matching prefixes, updating the data as we
    // traverse the tree, until we add a new leaf node or reach an existing one.
    // If the new leaf node has a common prefix with a sibling, then we combine the two under the
    // common prefix.
    private void insert(String time, String query) {
      // Increment count in current tree node.
      incrementQueryCount(query);

      // If the current tree node's prefix is equal to the time, then this is nothing left to do.
      if (this.prefix.equals(time)) return;

      // Search among children for matching prefix. If found, insert in matching child.
      // Note: there should only be at most one matching prefix.
      Map.Entry<String, PrefixTreeNode> possiblePrefixingChild = prefixToChildren.floorEntry(time);
      if (possiblePrefixingChild != null && time.startsWith(possiblePrefixingChild.getKey())) {
        // then this is indeed a prefix. We'll insert in this child.
        possiblePrefixingChild.getValue().insert(time, query);
      } else {
        // If not found, create new child.
        PrefixTreeNode newChild = new PrefixTreeNode(time);
        newChild.incrementQueryCount(query);

        // Next step: can we combine this new child with another child, such that they have a common
        // prefix that is longer than this.prefix?
        // There are only two possibilities: prefixToChildren.floorEntry(time) or prefixToChildren.ceilingEntry(time)
        // Every other child would already have been combined with one of these if they had a
        // common prefix longer than this.prefix
        Map.Entry<String, PrefixTreeNode> combineOptionLeft = possiblePrefixingChild; // floorEntry from before.
        Map.Entry<String, PrefixTreeNode> combineOptionRight = prefixToChildren.ceilingEntry(time);

        PrefixTreeNode childToCombine = null; // will be non-null if there is a child to combine
        String commonPrefix = null;
        if (combineOptionLeft != null) {
          commonPrefix = findCommonPrefix(combineOptionLeft.getKey(), time);
          if (commonPrefix.length() > prefix.length()) {
            childToCombine = combineOptionLeft.getValue();
          }
        }
        if (childToCombine == null && combineOptionRight != null) {
          commonPrefix = findCommonPrefix(combineOptionRight.getKey(), time);
          if (commonPrefix.length() > prefix.length()) {
            childToCombine = combineOptionRight.getValue();
          }
        }
        if (childToCombine != null) {
          Preconditions.checkNotNull(commonPrefix);

          // We can consolidate these children
          PrefixTreeNode newIntermediateNode = childToCombine.deepCopy(commonPrefix);

          // Increment the query in the new intermediate node. Add the other two nodes as children.
          newIntermediateNode.incrementQueryCount(query);
          newIntermediateNode.prefixToChildren.put(time, newChild);
          newIntermediateNode.prefixToChildren.put(childToCombine.prefix, childToCombine);

          // remove childToCombine from the current children, since we moved it under the new
          // intermediate node. Add the new intermediate node in the children map.
          this.prefixToChildren.remove(childToCombine.prefix);
          this.prefixToChildren.put(commonPrefix, newIntermediateNode);
        } else {
          // If there is no child to combine, that means that there is no common prefix among the children
          // longer than this.prefix. So we just add the newChild to the map of children
          this.prefixToChildren.put(time, newChild);
        }
      }
    }

    // Given a request prefix, traverses the tree to find a node with a prefix that includes the
    // request prefix.
    private PrefixTreeNode getNodeForPrefix(String requestPrefix) {
      Preconditions.checkState(requestPrefix.startsWith(this.prefix),
          "Current tree node's prefix (%s) must be a prefix of requestPrefix (%s)",
          this.prefix, requestPrefix);

      // Case 1: requestPrefix is a prefix of a child. In this case, we can just return the child,
      // because there will only be one child that can be prefixed by requestPrefix, otherwise
      // there would have been a merge between the multiple children.
      Map.Entry<String, PrefixTreeNode> possiblePrefixedChild =
          this.prefixToChildren.ceilingEntry(requestPrefix);
      if (possiblePrefixedChild != null && possiblePrefixedChild.getKey().startsWith(requestPrefix)) {
        return possiblePrefixedChild.getValue();
      }

      // Case 2: requestPrefix prefixed by a child. In this case, we continue our recursive search
      // in that child. requestPrefix cannot be prefixed by multiple children, otherwise those
      // children would have been merged.
      Map.Entry<String, PrefixTreeNode> possiblePrefixingChild =
          this.prefixToChildren.floorEntry(requestPrefix);
      if (possiblePrefixingChild != null && requestPrefix.startsWith(possiblePrefixingChild.getKey())) {
        return possiblePrefixingChild.getValue().getNodeForPrefix(requestPrefix);
      }

      // Case 3: No match.
      return null;
    }

    // Return the top {size} queries by count
    private List<QueryCount> top(int size) {
      List<QueryCount> result = new LinkedList<>();
      QueryListNode curr = countListHead;
      while (result.size() < size && curr != null) {
        result.add(new QueryCount(curr.query, curr.count));
        curr = curr.next;
      }
      return result;
    }

    // Return distinct count of queries for a node.
    private int count() {
      return this.queryCounts.size();
    }

    /**
     * Doubly linked list of queries with counts.
     * Non-static inner class so that the instances can access the head and tail references
     * from the parent PrefixTreeNode instance.
     */
    private class QueryListNode {
      private final String query;
      private int count;
      private QueryListNode prev;
      private QueryListNode next;

      private QueryListNode(String query, int count) {
        this.query = query;
        this.count = count;
        this.prev = null;
        this.next = null;
      }

      // move current node ahead of other node in linked list
      private void moveAheadOf(QueryListNode other) {
        Preconditions.checkState(this.count >= other.count, "Only move higher counts ahead");
        if (this.next != null) {
          this.next.prev = this.prev;
        }
        this.prev.next = this.next; // this.prev is not null because other is ahead in the list.
        if (this.prev.next == null) countListTail = this.prev;

        this.prev = other.prev;
        if (this.prev == null) {
          countListHead = this;
        } else {
          this.prev.next = this;
        }
        other.prev = this;
        this.next = other;
      }
    }

    private static String findCommonPrefix(String str1, String str2) {
      String shortestString;
      String longestString;
      if (str1.length() < str2.length()) {
        shortestString = str1;
        longestString = str2;
      } else {
        shortestString = str2;
        longestString = str1;
      }
      for (int i = shortestString.length(); i > 0; i--) {
        String prefix = shortestString.substring(0,i);
        if (longestString.startsWith(prefix)) {
          return prefix;
        }
      }
      return "";
    }
  }
}
