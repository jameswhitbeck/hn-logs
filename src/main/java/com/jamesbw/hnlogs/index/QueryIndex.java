package com.jamesbw.hnlogs.index;

import java.util.List;

public interface QueryIndex {

  /**
   * @param requestPrefix
   * @param size
   * @return top N (size) queries by count within the time range defined by the date prefix
   */
  List<QueryCount> getPopular(String requestPrefix, int size);

  /**
   * @param requestPrefix
   * @return distinct count of queries within the time range defined by the date prefix
   */
  int getCount(String requestPrefix);

  /**
   * @param time
   * @param query
   * Add a new entry to the index. May not be supported by all implementations.
   */
  void indexEntry(String time, String query);

  class QueryCount {
    public final String query;
    public final int count;

    public QueryCount(String query, int count) {
      this.query = query;
      this.count = count;
    }

    // equals() and hashCode() are overridden for testing purposes.
    @Override public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      QueryCount that = (QueryCount) o;

      if (count != that.count) return false;
      return query != null ? query.equals(that.query) : that.query == null;
    }

    @Override public int hashCode() {
      int result = query != null ? query.hashCode() : 0;
      result = 31 * result + count;
      return result;
    }
  }
}
