package com.jamesbw.hnlogs.index;

import com.jamesbw.hnlogs.index.DataLoader.DataEntry;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A simple implementation that pre-computes, for every possible prefix, a sorted list of queries
 * by count. This implementation does not support adding new elements to the index after the
 * pre-computation.
 */
public class PrecomputedPrefixHashMapQueryIndex implements QueryIndex {
  private final Map<String, List<QueryCount>> precomputedQueryCounts = new HashMap<>();

  public PrecomputedPrefixHashMapQueryIndex() {
    this(new DataLoader().loadData("hn_logs_1.tsv.gz", "hn_logs_2.tsv.gz"));
  }

  public PrecomputedPrefixHashMapQueryIndex(List<DataEntry> dataEntries) {
    long start = System.currentTimeMillis();

    // This map contains, for each prefix, a map of query to occurrence count
    Map<String, Map<String, Integer>> prefixToQueriesCounts = new HashMap<>();

    // First, we'll iterate through the data and build up prefixToQueriesCounts
    for (DataEntry dataEntry : dataEntries) {
      String time = dataEntry.timestamp;
      String query = dataEntry.query;

      // For each prefix, we'll increment the map of query counts
      for (int i = 0; i < time.length() + 1; i++) {
        String prefix = time.substring(0, i);
        if (!prefixToQueriesCounts.containsKey(prefix)) {
          prefixToQueriesCounts.put(prefix, new HashMap<String, Integer>());
        }
        Map<String, Integer> queryCounts = prefixToQueriesCounts.get(prefix);
        if (queryCounts.containsKey(query)) {
          queryCounts.put(query, queryCounts.get(query) + 1);
        } else {
          queryCounts.put(query, 1);
        }
      }
    }

    // Next, we transform the query counts maps in prefixToQueriesCounts to be order lists
    // so that we can easily support getPopular.
    for (String prefix : prefixToQueriesCounts.keySet()) {
      Map<String, Integer> queryCounts = prefixToQueriesCounts.get(prefix);
      List<QueryCount> queryCountList = new LinkedList<>();
      for (Map.Entry<String, Integer> queryCount : queryCounts.entrySet()) {
        queryCountList.add(new QueryCount(queryCount.getKey(), queryCount.getValue()));
      }
      Collections.sort(queryCountList, new Comparator<QueryCount>() {
        @Override public int compare(QueryCount o1, QueryCount o2) {
          return Integer.compare(o2.count, o1.count);
        }
      });
      precomputedQueryCounts.put(prefix, queryCountList);
    }
    long end = System.currentTimeMillis();
    System.out.println(String.format("Indexing finished for %s in %.3f seconds",
        this.getClass().getSimpleName(), (end - start) / 1000.0));
  }

  @Override public List<QueryCount> getPopular(String requestPrefix, int size) {
    if (precomputedQueryCounts.containsKey(requestPrefix)) {
      List<QueryCount> results = precomputedQueryCounts.get(requestPrefix);
      return results.subList(0, Math.min(size, results.size()));
    } else {
      return Collections.emptyList();
    }
  }

  @Override public int getCount(String requestPrefix) {
    if (precomputedQueryCounts.containsKey(requestPrefix)) {
      return precomputedQueryCounts.get(requestPrefix).size();
    } else {
      return 0;
    }
  }

  @Override public void indexEntry(String time, String query) {
    // This could be supported, but would require resorting the list for each prefix.
    throw new RuntimeException("::indexEntry is not supported on PrecomputedPrefixHashMapQueryIndex");
  }
}
