package com.jamesbw.hnlogs.validation;

public class DateValidator {

  private static final String DATE_PATTERN =
      "\\d\\d\\d\\d-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01]) ([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]";

  public boolean isValidDatePrefix(String datePrefix) {
    String mask = "0001-01-01 00:00:00";

    String paddedDate = datePrefix; // we pad the date with the mask, the validate against the valid pattern
    if (datePrefix.length() < mask.length()) {
      paddedDate = datePrefix + mask.substring(datePrefix.length());
    }
    return paddedDate.matches(DATE_PATTERN);
  }
}
