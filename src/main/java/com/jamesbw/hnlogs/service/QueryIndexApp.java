package com.jamesbw.hnlogs.service;

import com.jamesbw.hnlogs.jackson.PrettyPrintingObjectMapperProvider;
import org.glassfish.jersey.server.ResourceConfig;

public class QueryIndexApp extends ResourceConfig{
  public QueryIndexApp() {
    register(PrettyPrintingObjectMapperProvider.class);
  }
}
