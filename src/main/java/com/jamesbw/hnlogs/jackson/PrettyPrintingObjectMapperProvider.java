package com.jamesbw.hnlogs.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import javax.ws.rs.ext.ContextResolver;

// For nice indentation in the JSON responses.
public class PrettyPrintingObjectMapperProvider implements ContextResolver<ObjectMapper> {

  private final ObjectMapper defaultObjectMapper;

  public PrettyPrintingObjectMapperProvider() {
    defaultObjectMapper = createDefaultMapper();
  }

  @Override
  public ObjectMapper getContext(Class<?> type) {
    return defaultObjectMapper;
  }

  private static ObjectMapper createDefaultMapper() {
    final ObjectMapper result = new ObjectMapper();
    result.enable(SerializationFeature.INDENT_OUTPUT);
    return result;
  }
}
